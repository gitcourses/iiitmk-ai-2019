from Env import Environment
import numpy as np


def road_cross(**agents):
    env = Environment()
    counter = 0
    for name, func in agents.items():
        print(name, func)
        env.agents[name] = {
            "alive": True,
            "crossed": False,
            "dead": False,
            "pos": env.new_agent_pos(),
            "func": func,
            "success": 0,
        }

    no_of_trials = 10
    for i in range(no_of_trials):
        env.reset_road()
        env.reset_agents()
        env.visualize(env.road)
        while not env.ALL_DEAD:
            env.step(i)
    for name, agent in env.agents.items():
        print(f"{name} : {agent['success']}")


def test():
    def assignment_4(*args) -> bool:
        road = args[0]
        pos = args[1]
        vel = args[2]
        name = args[3]
        print(road, pos, vel, name)
        input()
        return np.random.choice(["U", "D", "L", "R", "O"])

    names = list("qwertyuiopasd")
    agents = {}
    for i in names:
        agents[i] = assignment_4
    road_cross(**agents)


test()
